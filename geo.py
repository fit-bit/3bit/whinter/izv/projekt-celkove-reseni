#!/usr/bin/python3.8
# coding=utf-8
import pandas as pd
import geopandas
import matplotlib.pyplot as plt
import contextily
import sklearn.cluster
import numpy as np
# muzeze pridat vlastni knihovny
import os


def make_geo(df: pd.DataFrame) -> geopandas.GeoDataFrame:
    """ Konvertovani dataframe do geopandas.GeoDataFrame se spravnym kodovanim"""

    # Vybrání dat pro jihomoravský kraj
    df.drop(df[df['region'] != 'JHM'].index, inplace = True)

    # Odstranení nehod kde nejsou souřadnice
    df.dropna(subset = ['d'], inplace = True)
    df.dropna(subset = ['e'], inplace = True)

    # Vytvoření geopandas rámce s křovákovím souřadnicovým systémem
    return geopandas.GeoDataFrame(df, geometry=geopandas.points_from_xy(df['d'], df['e']), crs="EPSG:5514")

def plot_geo(gdf: geopandas.GeoDataFrame, fig_location: str = None, show_figure: bool = False):
    """ Vykresleni grafu s dvemi podgrafy podle lokality nehody """

    # Přetransformování pro lepší zobrazení podkladové mapy
    gdf = gdf.to_crs("epsg:3857")

    # Vytvoření grafů
    fig, (ax1, ax2) = plt.subplots(1, 2, sharex = True, sharey = True, figsize=(15,6))

    ### V obci ###
    ax1.set_title("Nehody v Jihomoravském kraji: v obci")
    gdf[gdf['p5a'] == 1].plot(ax=ax1, markersize=3, alpha=0.2, color="red")
    contextily.add_basemap(ax1, crs=gdf.crs.to_string(), source=contextily.providers.Stamen.TonerLite) # přidání podkladu
    ax1.axis("off")

    ### Mimo obci ###
    ax2.set_title("Nehody v Jihomoravském kraji: mimo obec")
    gdf[gdf['p5a'] == 2].plot(ax=ax2, markersize=3, alpha=0.2, color="green")
    contextily.add_basemap(ax2, crs=gdf.crs.to_string(), source=contextily.providers.Stamen.TonerLite) # přidání podkladu
    ax2.axis("off")

    fig.tight_layout()

    # Uložení a zobrazení grafu podle parametrů
    if fig_location != None:
        if not os.path.exists(os.path.dirname(fig_location)):
            if(os.path.dirname(fig_location) != ""):
                os.makedirs(os.path.dirname(fig_location))

        fig.savefig(fig_location)

    if show_figure == True:
        plt.show()

def plot_cluster(gdf: geopandas.GeoDataFrame, fig_location: str = None, show_figure: bool = False):
    """ Vykresleni grafu s lokalitou vsech nehod v kraji shlukovanych do clusteru """

    # Přetransformování pro lepší zobrazení podkladové mapy
    gdf = gdf.to_crs("epsg:3857")

    ############# Vytvoření shluků ##########
    # Rozřazení bodů do daného počtu shlaků
    coords = np.dstack([gdf.geometry.x, gdf.geometry.y]).reshape(-1, 2)
    db = sklearn.cluster.MiniBatchKMeans(n_clusters=40).fit(coords)
    gdf_multi = gdf.copy()
    gdf_multi["cluster"] = db.labels_

    # Spojení jednotlivých bodů v jednotlivých clusterech
    gdf_multi = gdf_multi.dissolve(by="cluster", aggfunc={"p1": "count"}).rename(columns=dict(p1="cnt"))

    # VYtvoření bodů které budou na mapě reprezentovat všechny body v jendom clastru
    gdf_coords = geopandas.GeoDataFrame(geometry=geopandas.points_from_xy(db.cluster_centers_[:, 0], db.cluster_centers_[:, 1]))
    gdf_multi = gdf_multi.merge(gdf_coords, left_on="cluster", right_index=True).set_geometry("geometry_y")

    # Vytvoření grafu
    fig, ax = plt.subplots(1, 1, figsize=(15,8))

    ax.set_title("Nehody v Jihomoravském kraji")

    # Všechny nehody
    gdf.plot(ax=ax, markersize=1, alpha=0.3)

    # Nehody shlukované
    gdf_multi.plot(ax=ax, markersize=gdf_multi["cnt"] / 5, column="cnt", alpha=0.5, legend=True)

    # Nastavení limitu os pro lepší zobrazení bez volného místa kolem
    x1,x2 = ax.get_xlim()
    y1,y2 = ax.get_ylim()

    ax.set_xlim([x1 + 69000, x2 - 5000])
    ax.set_ylim([y1, y2 - 20000])

    contextily.add_basemap(ax, crs=gdf.crs.to_string(), source=contextily.providers.Stamen.TonerLite) # přidání podkladu
    ax.axis("off")

    fig.tight_layout()

    # Uložení a zobrazení grafu podle parametrů
    if fig_location != None:
        if not os.path.exists(os.path.dirname(fig_location)):
            if(os.path.dirname(fig_location) != ""):
                os.makedirs(os.path.dirname(fig_location))

        fig.savefig(fig_location)

    if show_figure == True:
        plt.show()

if __name__ == "__main__":
    # zde muzete delat libovolne modifikace
    gdf = make_geo(pd.read_pickle("accidents.pkl.gz"))
    plot_geo(gdf, "geo1.png", True)
    plot_cluster(gdf, "geo2.png", True)

