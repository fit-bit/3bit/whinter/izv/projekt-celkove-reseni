#!/usr/bin/python3.8
# coding=utf-8
"""
    Soubor pro vygenerování dat použitých ve zprávě.

    Autor: Matěj Kudera
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# Názvy výrobců 
manufacturers = {
1:  "ALFA-ROMEO",
2:	"AUDI",
3:	"AVIA",
4:	"BMW",
5:	"CHEVROLET",
6:	"CHRYSLER",
7:	"CITROEN",
8:	"DACIA",
9:	"DAEWOO",
10:	"DAF",
11:	"DODGE",
12:	"FIAT",
13:	"FORD",
14:	"GAZ, VOLHA",
15:	"FERRARI",
16:	"HONDA",
17:	"HYUNDAI",
18:	"IFA",
19:	"IVECO",
20:	"JAGUAR",
21:	"JEEP",
22:	"LANCIA",
23:	"LAND ROVER",
24:	"LIAZ",
25:	"MAZDA",
26:	"MERCEDES",
27:	"MITSUBISHI",
28:	"MOSKVIČ",
29:	"NISSAN",
30:	"OLTCIT",
31:	"OPEL",
32:	"PEUGEOT",
33:	"PORSCHE",
34:	"PRAGA",
35:	"RENAULT",
36:	"ROVER",
37:	"SAAB",
38:	"SEAT",
39:	"ŠKODA",
40:	"SCANIA",
41:	"SUBARU",
42:	"SUZUKI",
43:	"TATRA",
44:	"TOYOTA",
45:	"TRABANT",
46:	"VAZ",
47:	"VOLKSWAGEN",
48:	"VOLVO",
49:	"WARTBURG",
50:	"ZASTAVA",
51:	"AGM",
52:	"ARO",
53:	"AUSTIN",
54:	"BARKAS",
55:	"DAIHATSU",
56:	"DATSUN",
57:	"DESTACAR",
58:	"ISUZU",
59:	"KAROSA",
60:	"KIA",
61:	"LUBLIN",
62:	"MAN",
63:	"MASERATI",
64:	"MULTICAR",
65:	"PONTIAC",
66:	"ROSS",
67:	"SIMCA",
68:	"SSANGYONG",
69:	"TALBOT",
70:	"TAZ",
71:	"ZAZ",
72:	"BOVA",
73:	"IKARUS",
74:	"NEOPLAN",
75:	"OASA",
76:	"RAF",
77:	"SETRA",
78:	"SOR",
79:	"APRILIA",
80:	"CAGIVA",
81:	"ČZ",
82:	"DERBI",
83:	"DUCATI",
84:	"GILERA",
85:	"HARLEY",
86:	"HERO",
87:	"HUSQVARNA",
88:	"JAWA",
89:	"KAWASAKI",
90:	"KTM",
91:	"MALAGUTI",
92:	"MANET",
93:	"MZ",
94:	"PIAGGIO",
95:	"SIMSON",
96:	"VELOREX",
97:	"YAMAHA"
}

# Načtení a vybrání potřebných dat
df_all = (pd.read_pickle("accidents.pkl.gz"))[['p45a', 'p44', 'p13a', 'p13b']].dropna()

df_all.drop(df_all[df_all['p45a'] == 00].index, inplace = True)
df_all.drop(df_all[df_all['p45a'] == 98].index, inplace = True)
df_all.drop(df_all[df_all['p45a'] == 99].index, inplace = True)
df_all.drop(df_all[df_all['p45a'] == -1].index, inplace = True) # Neznámá hodnota

# Předělání kódů značek na jména pro lepší výpis
df_all = df_all.replace({"p45a": manufacturers})


""" 
    Závislost: Má značka auta vliv na to kolik její řidiči mají těžkých dopravních nehod.
"""
# Omezení dat je na osobní automobily
df_cars = df_all.loc[df_all['p44'] == 3]

# Rozdělění na lehké a těžké nehody a sečtení podle značek
df_light_accidents = df_cars.loc[(df_cars['p13a'] + df_cars['p13b']) == 0]
df_heavy_accidents = df_cars.loc[(df_cars['p13a'] + df_cars['p13b']) > 0]

df_light_accidents = df_light_accidents.groupby('p45a').count()
df_light_accidents = df_light_accidents.reset_index()

df_heavy_accidents = df_heavy_accidents.groupby('p45a').count()
df_heavy_accidents = df_heavy_accidents.reset_index()

# Vytvoření indexu pro dělení
df_light_accidents.set_index('p45a', inplace=True)
df_heavy_accidents.set_index('p45a', inplace=True)

# Doplnění značek s žádnou těžkou nehodou pro plný výpis
df_heavy_accidents = df_heavy_accidents.reindex(df_light_accidents.index, fill_value=0)

# Získání poměru a odstranění neznámích hodnot
df_heavy_accidents_ratio = (df_heavy_accidents['p44']/df_light_accidents['p44']).dropna()

# Zpravení indexu pro výpis
df_heavy_accidents_ratio = df_heavy_accidents_ratio.reset_index()
df_heavy_accidents_ratio = df_heavy_accidents_ratio.rename(columns={"p45a": "Znacka", "p44": "Pomer nehod"})

############ Vyzualizace závislosti ###########
# Seřazení sestupně
df_heavy_accidents_ratio = df_heavy_accidents_ratio.sort_values('Pomer nehod', ascending = False)

# Sloupcový graf podle značek
fig, ax= plt.subplots(1, 1, figsize=(15,5))
sns.set_style("whitegrid")

ax.set_title("Poměr těžkých a lehkých nehod podle výrobce automobilu")
ax = sns.barplot(data=df_heavy_accidents_ratio, ax=ax, x='Znacka', y='Pomer nehod', color="red", order=df_heavy_accidents_ratio['Znacka'], ci=None)
ax.set(ylabel='Poměr těžkých/lehkých nehod', xlabel='Značka vozidla')

# Otočení popisků na x ose
for item in ax.get_xticklabels():
    item.set_rotation(90)

fig.tight_layout()

fig.savefig("fig1.png")
plt.show()

# Tabulka s přesnými hodnotami pro top 5 značek
df_heavy_accidents_ratio_table = df_heavy_accidents_ratio.copy()
df_heavy_accidents_ratio_table.set_index('Znacka', inplace=True)
print("Značky automobilů s největším poměrem těžkých nehod")
print(df_heavy_accidents_ratio_table.head().to_csv(sep=';'))

# Výpis záznamů v nejvišších skupinách
print("Wartburg lehké nehody: "+str(df_light_accidents.at['WARTBURG', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['WARTBURG', 'p44']))
print("Trabant lehké nehody: "+str(df_light_accidents.at['TRABANT', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['TRABANT', 'p44']))
print("Isuzu lehké nehody: "+str(df_light_accidents.at['ISUZU', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['ISUZU', 'p44']))
print("Lancia lehké nehody: "+str(df_light_accidents.at['LANCIA', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['LANCIA', 'p44']))
print("Porsche lehké nehody: "+str(df_light_accidents.at['PORSCHE', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['PORSCHE', 'p44']))
print("Saab lehké nehody: "+str(df_light_accidents.at['SAAB', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['SAAB', 'p44']))

print("##################################")

""" 
    Závislost: Má značka motorky vliv na to kolik její řidiči mají těžkých dopravních nehod.
"""
# Omezení dat jen na motorky
df_motorbikes = df_all.loc[df_all['p44'] == 2]

# Rozdělění na lehké a těžké nehody a sečtení podle značek
df_light_accidents = df_motorbikes.loc[(df_motorbikes['p13a'] + df_motorbikes['p13b']) == 0]
df_heavy_accidents = df_motorbikes.loc[(df_motorbikes['p13a'] + df_motorbikes['p13b']) > 0]

df_light_accidents = df_light_accidents.groupby('p45a').count()
df_light_accidents = df_light_accidents.reset_index()

df_heavy_accidents = df_heavy_accidents.groupby('p45a').count()
df_heavy_accidents = df_heavy_accidents.reset_index()

# Vytvoření indexu pro dělení
df_light_accidents.set_index('p45a', inplace=True)
df_heavy_accidents.set_index('p45a', inplace=True)

# Doplnění značek s žádnou těžkou nehodou pro plný výpis
df_heavy_accidents = df_heavy_accidents.reindex(df_light_accidents.index, fill_value=0)

# Získání poměru a odstranění neznámích hodnot
df_heavy_accidents_ratio = (df_heavy_accidents['p44']/df_light_accidents['p44']).dropna()

# Zpravení indexu pro výpis
df_heavy_accidents_ratio = df_heavy_accidents_ratio.reset_index()
df_heavy_accidents_ratio = df_heavy_accidents_ratio.rename(columns={"p45a": "Znacka", "p44": "Pomer nehod"})

############ Vyzualizace závislosti ###########
# Seřazení sestupně
df_heavy_accidents_ratio = df_heavy_accidents_ratio.sort_values('Pomer nehod', ascending = False)

# Sloupcový graf podle značek
fig, ax= plt.subplots(1, 1, figsize=(10,5))
sns.set_style("whitegrid")

ax.set_title("Poměr těžkých a lehkých nehod podle výrobce motociklu")
ax = sns.barplot(data=df_heavy_accidents_ratio, ax=ax, x='Znacka', y='Pomer nehod', color="red", order=df_heavy_accidents_ratio['Znacka'], ci=None)
ax.set(ylabel='Poměr těžkých/lehkých nehod', xlabel='Značka vozidla')

# Otočení popisků na x ose
for item in ax.get_xticklabels():
    item.set_rotation(90)

fig.tight_layout()

fig.savefig("fig2.png")
plt.show()

# Tabulka s přesnými hodnotami pro top 5 značek
df_heavy_accidents_ratio_table = df_heavy_accidents_ratio.copy()
df_heavy_accidents_ratio_table.set_index('Znacka', inplace=True)
print("\nZnačky motociklů s největším poměrem těžkých nehod")
print(df_heavy_accidents_ratio_table.head().to_csv(sep=';'))

# Výpis vybraných záznamů
print("Husqvarna lehké nehody: "+str(df_light_accidents.at['HUSQVARNA', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['HUSQVARNA', 'p44']))
print("Hero lehké nehody: "+str(df_light_accidents.at['HERO', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['HERO', 'p44']))
print("Ducati lehké nehody: "+str(df_light_accidents.at['DUCATI', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['DUCATI', 'p44']))
print("Piaggio lehké nehody: "+str(df_light_accidents.at['PIAGGIO', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['PIAGGIO', 'p44']))
print("Malaguti lehké nehody: "+str(df_light_accidents.at['MALAGUTI', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['MALAGUTI', 'p44']))
print("Derbi lehké nehody: "+str(df_light_accidents.at['DERBI', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['DERBI', 'p44']))
print("Gilera lehké nehody: "+str(df_light_accidents.at['GILERA', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['GILERA', 'p44']))
print("Peugeot lehké nehody: "+str(df_light_accidents.at['PEUGEOT', 'p44'])+" těžké nehody: "+str(df_heavy_accidents.at['PEUGEOT', 'p44']))

# END doc.py